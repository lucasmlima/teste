package models;

import enums.EnumMovimentos;
import enums.EnumOrientacao;

public class Sonda {

    private Integer[] area;
    private Integer pos_x;
    private Integer pos_y;
    private EnumOrientacao orientacao;
    private EnumMovimentos enumMovimentos;
    private Sonda[] sondas;

    public Sonda[] getSondas() {
        return sondas;   
    }
    public void setSondas(Sonda[] sondas) {
        this.sondas = sondas;
    }

    public Sonda(Integer pos_x, Integer pos_y, EnumOrientacao orientacao) {
        this.pos_x = pos_x;
        this.pos_y = pos_y;
        this.orientacao = orientacao;
    }

    public Integer[] getArea() {
        return area;
    }

    public void setArea(Integer[] area) {
        this.area = area;
    }
    public Integer getPos_x() {
        return pos_x;
    }
    public void setPos_y(Integer pos_y) {
        this.pos_y = pos_y;  
    }
    public Integer getPos_y() {
        return pos_y;
    }
    public void setPos_x(Integer pos_x) {
        this.pos_x = pos_x;  
    }

    public EnumMovimentos getEnumMovimentos() {
        return enumMovimentos;
    }
    public void setEnumMovimentos(EnumMovimentos enumMovimentos) {
       this.enumMovimentos = enumMovimentos;
    }

    public EnumOrientacao getOrientacao() {
        return orientacao;
    }
    public void setOrientacao(EnumOrientacao orientacao) {
       this.orientacao = orientacao;
    }
}