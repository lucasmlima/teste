package services;

import java.util.ArrayList;
import java.util.Arrays;

import enums.EnumMovimentos;
import enums.EnumOrientacao;
import models.Sonda;

public class SondaMap {

    private Integer[][] area;
    private Sonda[] sondas;
   
    public Integer[][] getArea(){
        return area;
    }
    public void setArea(Integer[][] area) {
        this.area = area;
    }
   
    public void DefinirArea(Integer pos_a, Integer pos_b) {
        Integer[][] matriz = new Integer[pos_a][pos_b];
        this.setArea(matriz);
        this.setSondas(new Sonda[0]);   
    }
    private void setSondas(Sonda[] sondas) {
        this.sondas = sondas;
    }
    public Sonda[] getSondas() {
		return sondas;
	}
    public void NovaSonda(Integer pos_x, Integer pos_y, EnumOrientacao ori) {
        Sonda newSonda = new Sonda(pos_x, pos_y, ori);
        this.setSondas(this.add(getSondas(),newSonda));
    }
   
    private Sonda[] add(Sonda[] originalArray, Sonda newItemSonda) {
        ArrayList<Sonda> lst = new ArrayList<>(Arrays.asList(originalArray));
        lst.add(newItemSonda);

        Sonda[] newSondas= new Sonda[lst.size()];
        newSondas= lst.toArray(newSondas);

        return newSondas;
    }

    public void Movimentar(EnumMovimentos... movs) {
      
        Integer aux = this.getSondas().length;
        //Integer aux2= aux -1;
        Sonda sondas = this.getSondas()[aux -1];//última sonda
        for (EnumMovimentos mov : movs){
            if (mov.equals(EnumMovimentos.M)){
                EnumOrientacao ori = sondas.getOrientacao();
                if (ori.equals(EnumOrientacao.N)) {
                    Integer auxpos = sondas.getPos_y() + 1;
                    sondas.setPos_y(auxpos);
                } else if (ori.equals(EnumOrientacao.E)) {
                    Integer auxpos = sondas.getPos_x() + 1;
                    sondas.setPos_x(auxpos);
                }else if (ori.equals(EnumOrientacao.S)) {
                    Integer auxpos = sondas.getPos_y() -1;
                    sondas.setPos_y(auxpos);
                } else {
                    Integer auxpos = sondas.getPos_x() - 1;
                    sondas.setPos_x(auxpos);
                }
            }else{
                EnumOrientacao ori = this.getOrientacao(mov, sondas.getOrientacao());
                sondas.setOrientacao(ori);
            }

        }
    }

    

    public EnumOrientacao getOrientacao(EnumMovimentos mov, EnumOrientacao ori) {
        EnumOrientacao oriRet;
        if (ori.equals(EnumOrientacao.N)) {
            oriRet = mov.equals(EnumMovimentos.L) ? EnumOrientacao.W : EnumOrientacao.E;
        } else if(ori.equals(EnumOrientacao.E)) {
            oriRet = mov.equals(EnumMovimentos.L) ? EnumOrientacao.N : EnumOrientacao.S;
        } else if(ori.equals(EnumOrientacao.S)) {
            oriRet = mov.equals(EnumMovimentos.L) ? EnumOrientacao.E : EnumOrientacao.W;
        } else {
            oriRet = mov.equals(EnumMovimentos.L) ? EnumOrientacao.S : EnumOrientacao.N;
        }

        return oriRet;
    }

    // private Sonda[] add(Sonda[] originalArray, Sonda newItem) {
    //     return null;
    // }


}