package enums;

public enum EnumMovimentos {
    R('R'), 
    L('L'), 
    M('M');


    
    public char asChar(){
        return this.asChar;
    }

    private final char asChar;

    EnumMovimentos(char asChar){
        this.asChar = asChar;
    }

    public static EnumMovimentos getFromChar(char asChar) {
        if (asChar == 'R') {
            return EnumMovimentos.R;
        } else if(asChar == 'L'){
            return EnumMovimentos.L;
        }else{
            return EnumMovimentos.M;
        }
    
    }

}