package enums;

public enum EnumOrientacao {
    N("N"), 
    S("S"), 
    W("W"), 
    E("E");

    private String text;
    EnumOrientacao(String text){
        this.text = text;
    }

    public String getText(){
        return this.text;
    }

    public static EnumOrientacao getFromString(String text) {
        for (EnumOrientacao b : EnumOrientacao.values()){
            if(b.text.trim().equalsIgnoreCase(text.trim())){
                return b;
            }
        }
        return null;
    }

}

