package app;


import java.util.Scanner;
import java.util.regex.Pattern;

import enums.EnumMovimentos;
import enums.EnumOrientacao;
import services.SondaMap;

public class App {
    public static void main(String[] args) throws Exception {
        //System.out.println("Hello Java")
        //teste 
 
        Scanner s = new Scanner(System.in);
        int contador = 1;
       
        
        System.out.println("Entre com o ponto mais ao nordeste da planicie, separado por espaços:");
		Pattern d = s.delimiter();
		int x = s.nextInt();
        int y = s.nextInt(); 
        
        while (contador != 0) {
            System.out.println("Entre a posição da sonda ou 0 para encerrar a entrada:");
            Pattern d1 = s.delimiter();
            int p1 = s.nextInt();
            contador = p1;

            if (contador == 0) {
                System.out.println("programa encerrado");
                s.close();
            }
            else{
                int p2 = s.nextInt();
            String orie = s.nextLine();
            
            System.out.println("Entre com uma sequência de comandos (R, L, M)");
            // String EnumOrientacao = s.nextLine();
            String com = s.nextLine();
            char[] comlist = com.toCharArray();
    
            SondaMap sonda = new SondaMap();
    
            sonda.DefinirArea(x, y);
            sonda.NovaSonda(p1, p2, EnumOrientacao.getFromString(orie));
            for (char output : comlist) {
                        sonda.Movimentar(EnumMovimentos.getFromChar(output));
            }
                
            //sonda.Movimentar(EnumMovimentos.R, EnumMovimentos.M, EnumMovimentos.L, EnumMovimentos.M);
            
            System.out.println(sonda.getSondas()[0].getPos_x());
            System.out.println(sonda.getSondas()[0].getPos_y());
            System.out.println(sonda.getSondas()[0].getOrientacao());
            
            }
                                   
        }            
        
    }

  
}